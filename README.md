# Building Management Controller

EPICS module to provide communications to/from Siemens Desigo Building Management Controller.

* This is a module specific to a particular BMC.
* This module contains the database with records that EPICS will read/write to this BMC.
* This module requires a common **modbus** module that provide drivers config, etc. This is done using **Makefile**.

If you need to create a new module for EPICS to communicate to a BMC:

1. Create a copy of this module
2. Name accordingly
3. Modify database
